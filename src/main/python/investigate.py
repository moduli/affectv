from pyspark import SparkContext
from pyspark.sql import SparkSession


spark = SparkContext()


# Create SparkSession object
spark = SparkSession \
    .builder \
    .master("local[*]") \
    .appName("Python Spark Ingestor") \
    .getOrCreate()

file_input = "/Users/andrewking/Projects/affectv/src/test/resources/impressions_filtered/part-00000"
df = spark.read.csv(file_input)

print(df.select("os_version").distinct)
