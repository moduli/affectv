from pyspark import SparkContext
from pyspark.sql import SparkSession
from src.main.python.validator import Validator


validator = Validator()
spark = SparkContext()


# Create SparkSession object
spark = SparkSession \
    .builder \
    .master("local[*]") \
    .appName("Python Spark Ingestor") \
    .getOrCreate()


def get_file_input():
    file_input = "/Users/andrewking/Projects/affectv/src/test/resources/impressions.tsv"
    return file_input


# Create dataframe from impresssions.tsv
df = spark.read.option("header", "true").csv(get_file_input())


df.cache()
df.printSchema()
print(df.rdd.first())


# Filter each row
rdd = df.rdd.map(validator.filter_row)
print(rdd.first())


# Output filtered file to impressions_filtered directory
file_output = "/Users/andrewking/Projects/affectv/src/test/resources/impressions_filtered"
rdd.coalesce(1).saveAsTextFile(file_output)
