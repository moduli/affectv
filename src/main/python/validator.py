from pyspark.sql import Row


class Validator(object):
    def __init__(self):
        pass

    def filter_ts(self, ts):
        """
        ts - Make sure right format regex
        Handle for None
        :return: ts
        """
        try:
            return ts
        except:
            return ts

    def filter_domain(self, domain):
        """
        domain - Make sure right format regex
        :return: domain
        """
        try:
            return domain
        except:
            return domain

    def filter_city(self, city):
        """
        city
        :return:
        """
        try:
            if city in ["", None]:
                return None
            else:
                return city
        except:
            return city

    def filter_country(self, country):
        """
        country - [Row(country=None), Row(country='??'), Row(country='**'), Row(country='')]
        :return: country
        """
        try:
            if country in ["??", "**", "", None]:
                return None
            else:
                return country
        except:
            return country

    def filter_browser_version(self, browser_version):
        """
        browser_version
        :return:
        """
        try:
            return browser_version
        except:
            return browser_version

    def filter_os_version(self, os_version):
        """
        os_version - [Row(os_version='unknown'), Row(os_version=None), Row(os_version='')]
        :return:
        """
        try:
            if os_version in ["unknown", "", None]:
                return None
            else:
                return os_version
        except:
            return os_version

    def filter_device(self, device):
        """
        device - All to Upper
        :return:
        """
        try:
            return device.upper()
        except:
            return device

    def filter_row(self, row):
        """
        Expected schema row.__fields__ = ['ts', 'domain', 'city', 'country', 'browser_version', 'os_version', 'device']
        :param row:
        :return: filtered_row if schema consistent with expectations. Else return row
        """

        try:

            filtered_ts = self.filter_ts(row.ts)

            filtered_domain = self.filter_domain(row.domain)

            filtered_city = self.filter_city(row.city)

            filtered_country = self.filter_country(row.country)

            filtered_browser_version = self.filter_browser_version(row.browser_version)

            filtered_os_version = self.filter_os_version(row.os_version)

            filtered_device = self.filter_device(row.device)

            return Row(ts=filtered_ts,
                       domain=filtered_domain,
                       city=filtered_city,
                       country=filtered_country,
                       browser_version=filtered_browser_version,
                       os_version=filtered_os_version,
                       device=filtered_device)

        except AttributeError:
            print("Could not filter attribute. Returning default row")
            return row
