import pytest
from src.main.python.validator import Validator


validator = Validator()


def test():
    assert True


@pytest.mark.parametrize("input_, output_", [
    (None, None),
    ("2017-04-04 17:30:15", "2017-04-04 17:30:15")
])
def test_filter_ts(input_, output_):
    """
    ts - Make sure right format regex
    :return:
    """
    assert validator.filter_ts(input_) == output_


def test_filter_domain():
    """
    domain - Make sure right format regex
    :return:
    """
    assert True

def test_filter_city():
    """
    city
    :return:
    """
    assert True


@pytest.mark.parametrize("input_, output_", [
    (None, None),
    ("??", None),
    ("**", None),
    ("", None),
    ("abc", "abc")
])
def test_filter_country(input_, output_):
    """
    country - [Row(country='??'), Row(country='**')]
    :return:
    """
    assert validator.filter_country(input_) == output_


def test_filter_browser_version():
    """
    browser_version
    :return:
    """
    assert True


@pytest.mark.parametrize("input_, output_", [
    (None, None),
    ("unknown", None),
    ("", None),
    ("abc", "abc")
])
def test_filter_os_version(input_, output_):
    """
    os_version - [Row(os_version='unknown'), Row(os_version=None), Row(os_version='')]
    :return:
    """
    assert validator.filter_os_version(input_) == output_


@pytest.mark.parametrize("input_, output_", [
    (None, None),
    ("abc", "ABC"),
    ("", ""),
    ("ab1c", "AB1C")
])
def test_filter_device(input_, output_):
    """
    device - All to Upper
    :return:
    """
    assert validator.filter_device(input_) == output_
